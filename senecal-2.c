#include <stdio.h>
#include <pthread.h> 
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <unistd.h>

/* 
 * The producer consumer problem is solved using mutex locks. 
 * The buffer is implemented as a doubly linked list. 
 * The main function creates and runs the threads, one each for 2 producers, and 2 consumers, for 4 threads total 
 * 
 * I maintain a global timer so the program doesn't run forever. 
 * 
 * This code was tested on macOS Sierra
 * 
 * Compile this code with: gcc -o main senecal-2.c -lpthread 
 * 
 * Then run ./main
*/

// Global timer so program doesn't run forever
clock_t start;
clock_t diff;
clock_t maxTime = 3000000; // max time for program to run

// Create the mutex lock, and condition variables 
pthread_mutex_t lock;
pthread_cond_t condP1, condP2, condC1, condC2;

// list struct to track size and head of list
struct List {
    int size;
    struct Node* head;
};

// a node in a double linked list maintains references to the previous and next nodes 
struct Node {
    int payload;
    struct Node* next;
    struct Node* prev;
};

// function to initialize a doubly linked list 
struct List* createList() {
    struct List* l = (struct List*)malloc(sizeof(struct List));
    l->size = 0;
    l->head = NULL;
    return l;
};

void printList(struct List* l) {
    struct Node* head = l->head;
    while (head != NULL) {
        printf("%d ", head->payload);
        head = head->next;
    }
    printf("\n");
}

void append(struct List* l, int payload) {
    struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));
    l->size++;

    /* Insert payload into new node */
    newNode->payload = payload;

    /* New node is at end of list so next reference should be null */
    newNode->next = NULL;

    /* if list empty this node is the head */
    if (l->head == NULL) {
        newNode->prev = NULL;
        l->head = newNode;
        return;
    }

    /* otherwise move to the end of the list */
    struct Node* last = l->head;
    while (last->next != NULL) {
        last = last->next;
    }

    /* next of last node should be the new node */
    last->next = newNode;

    return;
}

void delete(struct List* l) {
    /* Make the head the next node in the list */
    if (l->head == NULL) {
        return;
    }

    struct Node* temp = l->head;
    l->head = temp->next;

    free(temp);
    l->size--;
    return;
}

void* producer1(void *arg) {
    struct List* l = arg;
    int i;

    // Seed random number generator
    srand(time(NULL));
    bool waitMessage = true;

    while (diff < maxTime) {
        diff = (clock() - start); /* timer so program doesn't run forever */
        sleep(rand() % 2 / 10);

        pthread_mutex_lock(&lock); /* protect the buffer */
        while (l->size > 30) {
            if (waitMessage) {
                printf("\nProducers waiting...\n\n");
                waitMessage = false;
            }
            pthread_cond_wait(&condP1, &lock); /* if buffer is full then wait */
        }

        // Generate random odd int in the range [0, 50)
        i = (rand() % 25) * 2 + 1;

        // Print the list contents and add the new node 
        printf("List before Producer 1 appends: ");
        printList(l);
        append(l, i);
        printf("List after Producer 1 appends: ");
        printList(l);
        printf("\n");

        // wake up other threads
        pthread_cond_signal(&condC1);
        pthread_cond_signal(&condC2);
        pthread_cond_signal(&condP2);

        // release the buffer
        waitMessage = true; 
        pthread_mutex_unlock(&lock);
    }
    pthread_exit(0);
}

void* producer2(void *arg) {
    struct List* l = arg;
    int i;

    // Seed random number generator
    srand(time(NULL));
    bool waitMessage = true;

    while (diff < maxTime) {
        diff = (clock() - start); /* timer so program doesn't run forever */

        pthread_mutex_lock(&lock); /* protect the buffer */
        while (l->size > 30) {
            if (waitMessage) {
                printf("\nProducers waiting...\n\n");
                waitMessage = false;
            }
            pthread_cond_wait(&condP2, &lock); /* if buffer is full then wait */
        }
        
        // Generate random int in the range [0, 50)
        i = (rand() % 26) * 2;

        // Print the list contents and add the new node 
        printf("List before Producer 2 appends: ");
        printList(l);
        append(l, i);
        printf("List after Producer 2 appends: ");
        printList(l);
        printf("\n");

        // wake up other threads
        pthread_cond_signal(&condC2);
        pthread_cond_signal(&condP1);
        pthread_cond_signal(&condC1);

        // release the buffer
        waitMessage = true;
        pthread_mutex_unlock(&lock);
    }
    pthread_exit(0);
}

void* consumer1(void *arg) {
    struct List* l = arg;
    bool waitMessage = true; 

    while (diff < maxTime) { /* timer so program doesn't run forever */
        sleep(rand() % 2 / 10);

        pthread_mutex_lock(&lock); /* protect the buffer */
        while ((l->size <= 0 || l->head->payload % 2 == 0) && diff < maxTime) { 
            if (waitMessage && l->size <=0) {
                printf("\nConsumers waiting...\n\n");
                waitMessage = false;
            }
            pthread_cond_wait(&condC1, &lock); /* if buffer is empty or head is even then wait */
        }

        // Print the list contents and perform the deletion
        printf("List before Consumer 1 deletes: ");
        printList(l);
        delete(l);
        printf("List after Consumer 1 deletes: ");
        printList(l);
        printf("\n");

        // Wake up other threads
        pthread_cond_signal(&condP1);
        pthread_cond_signal(&condP2);
        pthread_cond_signal(&condC2);

        // release the buffer
        waitMessage = true; 
        pthread_mutex_unlock(&lock);
    }
    pthread_exit(0);
}

void* consumer2(void *arg) {
    struct List* l = arg;
    bool waitMessage = true; 

    while (diff < maxTime) { /* timer so program doesn't run forever */
        sleep(rand() % 2 / 10);

        pthread_mutex_lock(&lock); /* protect the buffer */
        while ((l->size <= 0 || l->head->payload % 2 != 0) && diff < maxTime) { 
            if (waitMessage && l->size <=0) {
                printf("\nConsumers waiting...\n\n");
                waitMessage = false;
            }
            pthread_cond_wait(&condC2, &lock); /* if buffer is empty or head is odd then wait */
        }

        // Print list contents and perform the deletion
        printf("List before Consumer 2 deletes: ");
        printList(l);
        delete(l);
        printf("List after Consumer 2 deletes: ");
        printList(l);
        printf("\n");

        // Wake up other threads 
        pthread_cond_signal(&condP2);
        pthread_cond_signal(&condP1);
        pthread_cond_signal(&condC1);

        // release the buffer
        waitMessage = true; 
        pthread_mutex_unlock(&lock);
    }
    pthread_exit(0);
}

int main(int argc, char **argv) {
    struct List* l = createList();

    // insert the initial three nodes
    srand(time(NULL));
    append(l, rand() % 50);
    append(l, rand() % 50);
    append(l, rand() % 50);

    // initialize timer so program doesn't run forever
    start = clock();

    // initialize mutex and condition variables
    pthread_mutex_init(&lock, NULL);
    pthread_cond_init(&condC1, NULL);
    pthread_cond_init(&condC2, NULL);
    pthread_cond_init(&condP1, NULL);
    pthread_cond_init(&condP2, NULL);

    // create the threads
    pthread_t p1, p2, c1, c2;

    pthread_create(&c1, NULL, consumer1, l);
    pthread_create(&p1, NULL, producer1, l);
    pthread_create(&c2, NULL, consumer2, l);
    pthread_create(&p2, NULL, producer2, l);

    // wait for thread to finish
    pthread_join(c1, NULL);
    pthread_join(p1, NULL);
    pthread_join(c2, NULL);
    pthread_join(p2, NULL);
}